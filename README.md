# miniproj6_hw308
This project is an extension of [Mini project 5](https://gitlab.com/dukeaiml/IDS721/miniproj5_hw308), which deployed the lambda function with API gateway to let user post vehicle information to a table `vehicle` of the database DynamoDB. Meanwhile, it added logging to the Rust Lambda function and integrated AWS X-Ray tracing, and connected logs/traces to CloudWatch finally.


## Instructions

- Add logging to a Rust Lambda function. We need to add dependencies for tracing and tracing_subscriber in `Cargo.toml` and code for logging and tracing in `main.rs`, which format the logs in a human-readable way, and is suitable for development and production debugging.
```
// In the 'main' function
let subscriber = tracing_subscriber::FmtSubscriber::builder()
        .with_max_level(Level::INFO)
        .finish();
    tracing::subscriber::set_global_default(subscriber)
        .expect("Setting default subscriber failed");

```
```
// In the 'post_new_info' function
.await.map_err(|err| {
            tracing::error!("Error posting to DynamoDB: {}", err);
            LambdaError::from(err.to_string())
```

- Integrate AWS X-Ray tracing. First, we need to create a new IAM role with appropriate permissions(shown in screenshots below). Then we enable the X-ray active tracing under the `Configuration` section of lambda function we deployed.

- Connect logs/traces to CloudWatch. We create a test event with the correct Event JSON. Then we go to CloudWatch and you can check for the updated logging and tracing information.
```
//Input to Event JSON
{
  "id": "04",
  "vehicle": "Sports",
  "size": "40",
  "price": "20000"
}
```

## Screenshots for Required Steps

##### 1. Adding Code for Logging implementation and X-Ray tracing

![](images/dependencies.png)
![](images/logging.png)
![](images/tracing.png)

##### 2. Create new IAM role and enabling X-Ray tracing in AWS Lambda

![](images/new_role.png)
![](images/xray_tracing.png)

##### 3. CloudWatch centralization

![](images/cloudwatch.png)
![](images/logs.png)




